import random
import sys
import time

import pygame


class Game:
    """
    A class used to represent a Game.
    Main class to better understand code.

    Attributes
    ----------
    x : int
        Value for a game window width

    y : int
        Value for a game window height

    black : pygame.color()
        Representing color with pygame method including RGB values

    white : pygame.color()
        Representing color with pygame method including RGB values

    red : pygame.color()
        Representing color with pygame method including RGB values

    green : pygame.color()
        Representing color with pygame method including RGB values

    blue : pygame.color()
        Representing color with pygame method including RGB values

    food_spawn : bool
        Boolean value to enable food spawning on the game map

    food_pos : list
        Position of the snake's food on the map.

    score : int
        Player's score

    speed : int
        Game speed used by a pygame time clock

    Methods
    ----------------
    check_for_errors(pygame.init)
        Static method checking if any error occurred, taking as a parameter pygame initialize.

    game_over()
        Method to display game over screen

    show_score()
        Method to display score while in the game and on the game over screen.

    spawn_food()
        Spawning food on the screen if there is not any.

    """

    def __init__(self, x, y):
        """
        Parameters
        ----------
        x : int
            Value for a game window width

        y : int
            Value for a game window height

        """
        # size of the screen
        self.x = x
        self.y = y
        # Colors (R, G, B)
        self.black = pygame.Color(0, 0, 0)
        self.white = pygame.Color(255, 255, 255)
        self.red = pygame.Color(255, 0, 0)
        self.green = pygame.Color(0, 255, 0)
        self.blue = pygame.Color(0, 0, 255)
        # making sure food is spawning
        self.food_spawn = True
        # position fo the food // is a floor division
        self.food_pos = [random.randrange(1, (self.x // 10)) * 10, random.randrange(1, (self.y // 10)) * 10]
        # score variable
        self.score = 0
        # speed of the snake
        # 10 - slow
        # 25 - pretty fast
        # 50 - fast
        self.speed = 20
        self.start_game = False

    # pygame.init() example output -> (6, 0)
    # second number in tuple gives number of errors
    @staticmethod
    def check_for_errors(pygame_init):
        """
        Method to check if any errors occurred

        Parameters
        ----------
        pygame_init : pygame.init()
            Value to initialize pygame
        """
        if pygame_init[1] > 0:
            print(f'[!] Had {pygame_init[1]} errors when initialising game, exiting...')
            sys.exit(-1)
        else:
            print('[+] Game successfully initialised')

    def title_screen(self):
        while not self.start_game:
            pressed_keys = pygame.key.get_pressed()
            for event in pygame.event.get():
                alt_f4 = (event.type == pygame.KEYDOWN and (event.key == pygame.K_F4
                                                            and (pressed_keys[pygame.K_LALT] or
                                                                 pressed_keys[pygame.K_RALT])
                                                            or event.key == pygame.K_q or event.key == pygame.K_ESCAPE))
                if event.type == pygame.QUIT or alt_f4:
                    sys.exit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        self.start_game = True
            my_font = pygame.font.SysFont('comicsansms', 90)
            title_screen_surface = my_font.render('Press SPACE to start', True, snakeGame.green)
            title_screen_rect = title_screen_surface.get_rect()
            title_screen_rect.center = (snakeGame.x / 2, snakeGame.y / 4)
            game_window.fill(snakeGame.black)
            game_window.blit(title_screen_surface, title_screen_rect)
            pygame.display.flip()
        return self.start_game

    def game_over(self):
        """
        Method to display game over screen
        """
        my_font = pygame.font.SysFont('comicsansms', 90)
        game_over_surface = my_font.render('Game Over', True, snakeGame.green)
        # drawing game over
        game_over_rect = game_over_surface.get_rect()
        game_over_rect.midtop = (snakeGame.x / 2, snakeGame.y / 4)
        game_window.fill(snakeGame.black)
        game_window.blit(game_over_surface, game_over_rect)
        self.show_score(0, snakeGame.green, 'comicsansms', 20)
        pygame.display.flip()
        # stuck game over for 3 seconds
        time.sleep(3)
        # show title screen
        self.start_game = False
        draw()
        return self.start_game

    # Score
    def show_score(self, option, color, font, size):
        """
        Method to display score

        Parameters
        ----------
        option : __eq__
            Value to decide if method is used to display in-game screen or game over summary
        color: pygame.Color()
            Color of the score
        font: Any
            Font of the score
        size: Any
            Font of the score
        """
        score_font = pygame.font.SysFont(font, size)
        score_surface = score_font.render('Score : ' + str(snakeGame.score), True, color)
        score_rect = score_surface.get_rect()
        # option 1 is displaying score while in a game
        if option == 1:
            score_rect.midtop = (self.x / 10, 15)
        # else display score after lost
        else:
            score_rect.midtop = (self.x / 2, self.y / 1.25)
        game_window.blit(score_surface, score_rect)

    def spawn_food(self):
        """
        Method to check if there is food on the map, if not spawning new at a random location.
        """
        # Spawning food on the screen
        # if there is not food on the map
        if not self.food_spawn:
            # spawn another food at random location
            self.food_pos = [random.randrange(1, (self.x // 10)) * 10, random.randrange(1, (
                    self.y // 10)) * 10]
        self.food_spawn = True
        return self.food_pos, self.food_spawn


class Snake:
    """
    Class snake to represent in-game snake object

    Attributes
    ----------
    snake_head : list
        Snake head position
    snake_body : list
        Snake body position, including head as a block.
    direction : str
        Initial direction of the snake
    change_to : any
        Attribute to change snake's position on click

    """

    def __init__(self):
        # Snake's body is cointaing 3 pixels at the start(head, 2 body pixels)
        # head position X,Y
        self.snake_head = [100, 50]
        # rest of the body position (including body-head block)
        self.snake_body = [[100, 50], [100 - 10, 50], [100 - (2 * 10), 50]]
        # initial direction of the snake
        self.direction = 'RIGHT'
        self.change_to = self.direction

    def snake_growing(self, gameobject):
        """
        This method is inserting new block at the snake's head position.
        If snake's head touch the food at x and y value game score is rising.

        """
        # Snake body growing mechanism
        try:
            # insert another block object at 0 position of the list
            self.snake_body.insert(0, list(self.snake_head))
            # if snake head position XY is similar to food position XY
            if self.snake_head[0] == gameobject.food_pos[0] and self.snake_head[1] == gameobject.food_pos[1]:
                gameobject.score += 1
                gameobject.food_spawn = False
            else:
                # remove last item from the list
                self.snake_body.pop()
        except IndexError:
            print("something went wrong/snake body out of range!")
        return gameobject.score, gameobject.food_spawn, self.snake_body


# ---Variables for the game---
# object for the game
snakeGame = Game(1024, 720)
# object for the snake
mySnake = Snake()
# FPS (frames per second) controller
fps_controller = pygame.time.Clock()

# Initialise game window with name and dimensions
pygame.display.set_caption('Snake')
game_window = pygame.display.set_mode((snakeGame.x, snakeGame.y))
gameIcon = pygame.image.load('snake.png')
pygame.display.set_icon(gameIcon)

# initializing all pygame modules
check_errors = pygame.init()

# check if errors occurred
snakeGame.check_for_errors(check_errors)


def main():
    while True:
        for event in pygame.event.get():
            # ---event for leaving the game by mouse X---
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            # Whenever a key is pressed down
            elif event.type == pygame.KEYDOWN:
                # W -> Up; S -> Down; A -> Left; D -> Right
                if event.key == pygame.K_UP or event.key == ord('w'):
                    mySnake.change_to = 'UP'
                elif event.key == pygame.K_DOWN or event.key == ord('s'):
                    mySnake.change_to = 'DOWN'
                elif event.key == pygame.K_LEFT or event.key == ord('a'):
                    mySnake.change_to = 'LEFT'
                elif event.key == pygame.K_RIGHT or event.key == ord('d'):
                    mySnake.change_to = 'RIGHT'
                # Esc -> Create event to quit the game
                elif event.key == pygame.K_ESCAPE:
                    pygame.event.post(pygame.event.Event(pygame.QUIT))

        # ---Making sure the snake cannot move in the opposite direction---
        if mySnake.change_to == 'UP' and mySnake.direction != 'DOWN':
            mySnake.direction = 'UP'
        if mySnake.change_to == 'DOWN' and mySnake.direction != 'UP':
            mySnake.direction = 'DOWN'
        if mySnake.change_to == 'LEFT' and mySnake.direction != 'RIGHT':
            mySnake.direction = 'LEFT'
        if mySnake.change_to == 'RIGHT' and mySnake.direction != 'LEFT':
            mySnake.direction = 'RIGHT'

        # ---Moving the snake---
        # changing y value by 1 block
        if mySnake.direction == 'UP':
            mySnake.snake_head[1] -= 10
        if mySnake.direction == 'DOWN':
            mySnake.snake_head[1] += 10
        # changing x value by 1 block
        if mySnake.direction == 'LEFT':
            mySnake.snake_head[0] -= 10
        if mySnake.direction == 'RIGHT':
            mySnake.snake_head[0] += 10

        # ---Snake growing mechanics---
        mySnake.snake_growing(snakeGame)
        snakeGame.spawn_food()

        # ---Drawing snake body and food as a rectangles 10x10---
        game_window.fill(snakeGame.black)
        for pos in mySnake.snake_body:
            # Snake body
            pygame.draw.rect(game_window, snakeGame.green, pygame.Rect(pos[0], pos[1], 10, 10))

        # Snake food
        pygame.draw.rect(game_window, snakeGame.red,
                         pygame.Rect(snakeGame.food_pos[0], snakeGame.food_pos[1], 10, 10))

        # ---Game Over conditions---
        # Getting out of bounds
        if mySnake.snake_head[0] < 0 or mySnake.snake_head[0] > snakeGame.x - 10:
            snakeGame.game_over()
        if mySnake.snake_head[1] < 0 or mySnake.snake_head[1] > snakeGame.y - 10:
            snakeGame.game_over()
        # Touching the snake body
        for block in mySnake.snake_body[1:]:
            if mySnake.snake_head[0] == block[0] and mySnake.snake_head[1] == block[1]:
                snakeGame.game_over()

        snakeGame.show_score(1, snakeGame.white, 'comicsansms', 20)
        # Refresh game screen
        pygame.display.update()
        # Refresh rate, speed of the game
        fps_controller.tick(snakeGame.speed)


# draw title screen
def draw():
    snakeGame.title_screen()
    # draw what would appear on the main menu
    if snakeGame.start_game:
        main()
        # draw what would appear while playing the game

    pygame.display.flip()


# initialize game
draw()

# special variable (is this file running directly by python?)
if __name__ == "__main__":
    main()
